FROM openjdk:9-jre as runtime
WORKDIR /srv
COPY    . .
RUN     ./gradlew test build
ENV SERVER_PORT 8080

EXPOSE ${SERVER_PORT}
HEALTHCHECK --interval=10s --timeout=3s \
	CMD curl -v --fail http://localhost:${SERVER_PORT}/ || exit 1

ENTRYPOINT ["java", "-jar", "build/libs/spring-boot-0.0.1-SNAPSHOT.jar"]