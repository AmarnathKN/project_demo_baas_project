---
noteId: "678fb2301f7411eb920da72db8e4279f"
tags: []

---

# CICD Deployment of Sample SpringBoot Application on Istio

## 1. Overview
This document will walk-you through the Continuous Integration/Continuous Deployment (CI/CD) process of deploying a sample hello world application on top of AWS EKS cluster.

### Tech Stack:

1) Version Control - Git(We use Bitbucket)

2) Continuous Integration - Jenkins

3) Continuous Deployment - Spinnaker(https://spinnaker.io/)

4) SpringBoot Build Tool - Gradle

5) Testing - JUnit 

6) Code Analysis - Sonarqube

7) Containerisation - Docker(AWS ECR)

8) Orchestartion - Kubernetes + Helm + Istio(Kubernetes ServiceMesh)


## 2. Creating and Building the App Locally

### Prerequistes:
To test the sample app locally, install the following tools:

1) Docker

2) aws-iam-authenticator

3) gradle 

### Follow the steps below to build the app locally:

Step1: Clone the repo locally:

```git clone git@bitbucket.org:deloittedigibank/sample-gradle-app-version2.git ```

Step2: ```cd sample-gradle-app-version2 ```

Step3: Run gradle commands to build the app

```gradle clean```

```gradle build ```

Step4: Run Dockerfile to test

Build Docker image for your sample app

``` docker build -t sample-app/hello-world . ```

Run Docker Image in your localhost

```docker run -p 8080:8080 sample-app/hello-world```

Now, go to your browser and search http://localhost:8080/, you should should see Hello world response like below: ![alt text](images/helloworld.png) 

NOTE: I would recommend build your own sample app and follow these steps

## 3. Continous Integration - Jenkins

In real-world, Devs will have go through the Jenkins Build Process to compile, build, test and containerized the app. 
In this section, we will explain how to perform Continous Integration of your app via Jenkins. 

### Prerequistes:

1) Jenkins Access - contact (somgarg@deloitte.com)

2) Sonarqube Access - contact (somgarg@deloitte.com)

3) AWS Client VPN(download locally)- https://aws.amazon.com/vpn/client-vpn-download/
  - You will need a openvpn config file to add a profile in AWS Client VPN
  - Follow these steps to add a profile in openvpn client 
    - windows: https://docs.aws.amazon.com/vpn/latest/clientvpn-user/client-vpn-connect-windows.html
    - macOS: https://docs.aws.amazon.com/vpn/latest/clientvpn-user/client-vpn-connect-macos.html
  

### Architecture: 

This is the high-level architecture diagram of the App Jenkins Pipeline:![alt text](images/jenkins.png) 

Detailed view of steps we are doing in jenkins pipeline:![alt text](images/jenkins-appview.png) 
(These steps are subject-to change in future as we will adding more integrations in the pipeline)

Follow these steps to build your own Jenkins Pipeline and modify Jenkinsfile in your codebasee:
Lets build your own Jenkins Pipeline and do CI of your application

### Step1: Create a Jenkins Pipeline: (Need Jenkins access)

NOTE: You need to be on VPN in order to access the Jenkins UI

- If you have Jenkins Access, login with your username and password

- Click on New Item on the left-side: ![alt text](images/jenkins/jenkins1.png) 
- A new screen will pop-up, follow the same nomenclature to name your pipeline as in the screenshot below:![alt text](images/jenkins/jenkins2.png) 
```$name-sample-pipeline-app${number}```
- Select Pipeline column and click OK
- A new screen will pop-up, check the boxes as in the screenshot below, in the pipeline section make sure to add the path of the Repository Path
like ```git@bitbucket.org:deloittedigibank/whatever-the-name-of-your-repo```
- Click Save

![alt text](images/jenkins/jenkins3a.png) 

![alt text](images/jenkins/jenkins3b.png) 

Congrats, Now you have a fully-functioning Jenkins Pipeline.

### Step2: Modify Jenkinsfile Locally:

Lets take a look at Jenkinsfile Code - Its written in Groovy

Follow the comments in environment{} and in stage {stage('Gradle App Helm package')}
section and make changes locally 

``` groovy
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException
env.ms_teams_url = "https://outlook.office.com/webhook/df89cacf-a047-4e9a-8dd1-db6a572307c0@36da45f1-dd2c-4d1f-af13-5abe46b99921/JenkinsCI/51facbdb3b894fcdac9b50ec53d68f85/814d8929-b5b6-4483-93af-2b5d8562b035"
def gradlew(String... args) {
    sh "./gradlew ${args.join(' ')} -s"
}
pipeline
{
  agent any
  triggers {
        pollSCM('* * * * *')
  }
  options {
       office365ConnectorWebhooks([[
                   startNotification: true,
                       url: 'https://outlook.office.com/webhook/df89cacf-a047-4e9a-8dd1-db6a572307c0@36da45f1-dd2c-4d1f-af13-5abe46b99921/JenkinsCI/51facbdb3b894fcdac9b50ec53d68f85/814d8929-b5b6-4483-93af-2b5d8562b035'
           ]]
        )
    }
  environment {
        AWS_CREDENTIALS = """${sh(returnStdout: true, script: 'aws sts assume-role --role-arn arn:aws:iam::054273887971:role/opendata_jenkins_admin --role-session-name "jenkins_connection"').trim()}"""
        REGISTRY_ENDPOINT = "054273887971.dkr.ecr.us-east-1.amazonaws.com/opendata-demo"
        registryCredential = 'ecr:us-east-1:ecr'
        //Change the Image Name as "your-app-name-version${Number}"
        IMAGE    = "hello-gradle-version2"
        //Change the Image Tag as "your-app-name-version${Number}"
        IMAGE_TAG = "hello-gradle-version2-${env.BUILD_NUMBER}"
        ECR_LOGIN = """${sh(returnStdout: true, script: 'aws ecr get-login-password').trim()}"""
    }
  stages
    {
        stage('Set assume role credentials')
        {
            steps
            {
		       script{
                    env.AWS_ACCESS_KEY_ID="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.AccessKeyId\'|tr -d \'"\'').trim()}"""
                    env.AWS_SECRET_ACCESS_KEY="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.SecretAccessKey\'|tr -d \'"\'').trim()}"""
                    env.AWS_SESSION_TOKEN="""${sh(returnStdout: true,script: 'set +x && echo $AWS_CREDENTIALS|jq \'.Credentials.SessionToken\'|tr -d \'"\'').trim()}"""
                    env.AWS_DEFAULT_REGION="us-east-1"
              }
            }
        }
        stage('gradle Compile') {
            steps {
                sh './gradlew clean'
            }
        }
        stage('gradle Build'){
          steps {
              sh './gradlew build'
          }
        }
         stage('JUnit Test') {
          steps {
            gradlew('clean', 'test')
          }
        }
          stage('Publish Junit Results'){
          steps {
            retry(3) {
              junit 'build/test-results/test/*.xml'
            }
          }
        }
        stage('Code Quality') {
          steps {
            script {
              try{
                checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
              }catch(e){
                echo e
              }
            }
          }
        }
        stage('SonarQube analysis') {
          steps {
              withSonarQubeEnv('sonarqube') { // Will pick the global server connection you have configured
                 gradlew('clean', 'sonarqube')
              }
            }
        }
        stage('Build Docker Image') {
          steps {
            echo 'Build Images'
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
                    withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                          sh '''
                            set +x aws ecr get-login-password
                            docker build -t ${IMAGE} .
                          '''
                        }
                    }
                  }
              }
        }
          stage('Tag Docker Image') {
          steps {
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
                   withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                    sh '''
                      docker tag ${IMAGE}:latest ${REGISTRY_ENDPOINT}:${IMAGE_TAG}
                    '''
              }
              }
            }
        }
        }
        stage('Waiting for Approval')
        {
            steps
            {
                office365ConnectorSend webhookUrl: "$ms_teams_url",
                    message: "Waiting for Approval ${env.JOB_NAME} Build #${env.BUILD_NUMBER} commited by @${user} [View on Jenkins](${env.JOB_URL})<br>Pipeline duration: ${currentBuild.durationString}",
                    status: 'Waiting for Approval!',
                    factDefinitions: [[name: "Test By", template: user]]
                input "Approve to Apply?"
            }
        }
        stage('Publish Docker Image to ECR') {
          steps {
            echo 'Push Images'
            script {
              docker.withRegistry("${REGISTRY_ENDPOINT}") {
               withCredentials([string(credentialsId: 'ecr', variable: 'SECRET')]) { //set SECRET with the credential content
                sh '''
                 docker login -u AWS -p ${ECR_LOGIN} https://${REGISTRY_ENDPOINT}
                 docker push ${REGISTRY_ENDPOINT}:${IMAGE_TAG}
                '''
              }
              }
            }
          }
        }
      stage('Gradle App Helm S3 init')
        {
            steps
            {
		     //Change helm repo add helm-chart-prodv2 s3://opendata-backend-state/charts/${add-your-name}
             //example: helm repo add helm-chart-prodv2 s3://opendata-backend-state/charts/somya
                sh '''
                    helm s3 init s3://opendata-backend-state/charts
                    helm repo add helm-chart-prodv2 s3://opendata-backend-state/charts
                '''
            }
        }
        stage('Gradle App Helm package')
        {
            steps
            {
		        sh '''
                    helm package charts/helm-chart-prodv2
                '''
            }
        }
        stage('Gradle App Helm deploy chart to S3')
        {
            steps
            {
		        sh '''
                  helm s3 push gradle-helm-chart-v2-0.1.0.tgz helm-chart-prodv2 --force
                '''
            }
        }
    }
}
```

## 4. Orchestrating Sample Application

We are using Kubernetes as our Orchestrating platform for Applications. The application will be managed/deployed on top of Istio(servicemesh)

We are using Helm tool to write/manage the kubernetes application

### Prerequistes:
1) Install Helm, kubernetes plugin in IDE

Before, moving forward, I would highly recommend go-through this tutorial step-by-step
https://kubernetes.io/docs/tutorials/hello-minikube/

This will help you understand a very basic high-level knowledge of Kubernetes

### Istio
We are using Istio as a Orchestartion servicemesh layer. This is where all your application will run. We are heavily using it

### Overview of Istio configuration
The Istio Configuration is made up of a number of key configuration objects detailed below:

Gateway - Matching incoming hosts to an ingressgateway. Manages SSL Termination

Virtual Services - Bound to a gateway and further to hosts manage all routing from the ingressgateway to the individual k8s services. Can further be used to do routing based on path / method / headers.
Role based MTLS (ServiceRole & ServiceRoleBinding) Allows Role based MTLS between services. Default behaviour is to deny unapproved M-TLS connections unless explicitly approved

Service Entries - All external network access is routed through an egress controller and should be configued using a service entry to allow egress

Destination Rules - Any service which requires non MTLS connections such as elastic search or Kafka should be defined as a Destination rule

A high level overview of this is detailed below:

![alt text](images/istio-arch.png)


Now, lets move forward and make few changes to our Application helm-chart

### Step1: cd charts/helm-chart-prodv2

#### Update the following values in `values.yaml`

namespace: yourname-appname-version-number

image:
  repository: 054273887971.dkr.ecr.us-east-1.amazonaws.com/opendata-demo:your-app-name-version${Number}-${ parameters.BUILD_NUMBER }

service:
  name: yourname-appname

```yaml
# Default values for springboot-demoweb.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.
replicaCount: 1
namespace: yourname-appname-version-number
image:
  repository: 054273887971.dkr.ecr.us-east-1.amazonaws.com/opendata-demo:your-app-name-version${Number}-${ parameters.BUILD_NUMBER }
  # tag: v1
  pullPolicy: IfNotPresent
service:
  name: opendata
  type: ClusterIP
  port: 8080
ingress:
  enabled: false
  # Used to create an Ingress record.
  hosts:
    - chart-example.local
  annotations:
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  tls:
    # Secrets must be manually created in the namespace.
    # - secretName: chart-example-tls
    #   hosts:
    #     - chart-example.local
resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #  cpu: 100m
  #  memory: 128Mi
  # requests:
  #  cpu: 100m
  #  memory: 128Mi
```

#### Update Virtual Service in `values.yaml`

```cd templates```
```Update virtual-service.yaml```

Update the following syntax below: 

`In line 15`, prefix: "/add-your-application-route"

`In line 20`, host: "add-your-application-route.hello-v2.svc.cluster.local"

save the code and push it to Bitbucket, as soon as the changes are pushed, your jenkins pipeline will run and run the jenkins steps

### At this point all your application CI steps are done


## 4. Continous Deployment - Spinnaker

For our Continous Deployment process, we are using spinnaker. Spinnaker is a Open-source project that was developed by Netflix and is highly recommended tool for doing Kubernetes Deployments

In this section, we will explain how to perform Continous Deployment of your apps via Spinnaker. 

### Prerequistes:

1) Spinnaker Access - contact (somgarg@deloitte.com)

2) AWS Sandbox Access - contact (macorley@deloitte.com)/(somgarg@deloitte.com)

### Architecture: 

This is the high-level architecture diagram of the CICD pipeline:![alt text](images/spinnaker/spinnakerarch.png) 

Detailed view of steps we are doing in Spinnaker pipeline:![alt text](images/spinnaker/spinnaker-arch.png)
(These steps are subject-to change in future as we will adding more integrations in the pipeline)

Follow these steps to build your own Spinnaker Pipeline via Spinnaker UI: Lets build your own Spinnaker Pipeline and do CD of your application

### Step1: Create a Spinnaker Pipeline: (Need Spinnaker access)

If you have Spinnaker Access, login with your username and password

- Click on Applications on top and select 'dev' application

![alt text](images/spinnaker/spinnaker1.png)

- Click on Pipeline tab and select on Click on 'configure a new Pipeline'

![alt text](images/spinnaker/spinnaker2.png)

- Name your pipeline as: yourname-yourappname-appversion

![alt text](images/spinnaker/spinnaker3.png)

-  Add Jenkins Trigger and your jenkins job from the dropdown, follow screenshot below:

![alt text](images/spinnaker/spinnaker4.png)

-  Add the following Parameter in the parameter section

![alt text](images/spinnaker/spinnaker5.png)

- Click on add stage tab on top and select 'Bake Manifest' as 'Type' from the dropdown and follow exact same steps in the screeenshot

![alt text](images/spinnaker/spinnaker6.png)

-  Add your app helm artifact, click on 'Add Artifact' and select 'Opendata-s3' artifact

![alt text](images/spinnaker/spinnaker7.png)

Make sure in the Objectpath: you add the exact same path as you put in Jenkins stage, just replace your name after charts/

s3://opendata-backend-state/charts/somya/gradle-helm-chart-v2-0.1.0.tgz

- Click on add stage again and Add a 'Deploy Manifest' stage, follow the steps in the screenshot and select the following attributes and click save

![alt text](images/spinnaker/spinnaker8.png)

![alt text](images/spinnaker/spinnaker9.png)
Note: this base64 artifact will be randomly generated artifact from stage1, so select whatever its showing in the dropdown 

Congrats, you have created a spinnaker Pipeline for your sample app 

## 5. Accessing your application from Browser

Now, your CICD Application is complete and you can test it in browser. 

To test it again, make change in your codebase, jenkins will automatically pick up the change and start the Jenkins Build, as soon as the build is complete, spinnaker pipeline will get triggered automatically. 

As soon as the spinnaker deployment is complete, you should be able to access the application via
API Gateway Custom Domain: 

Go to: https://sample-app.api-dev.us.deloitteopendata.com/opendata

/opendata -> will be replaced by your Virtual service route ```add-your-application-route```
https://sample-app.api-dev.us.deloitteopendata.com/add-your-application-route

NOTE: Make sure to open this link in Incognito/Private Window

![alt text](images/app.png)